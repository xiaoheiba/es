<?php


namespace App\service;

use Elasticsearch\ClientBuilder;
class Es
{
    public $client;
    public function __construct()
    {
        $this->client = ClientBuilder::create()->setHosts(['127.0.0.1:9200'])->build();
    }
    public function create_index(){
        //创建es实例
        $es = ClientBuilder::create()->setHosts(['127.0.0.1:9200'])->build();
        $params = [
            'index' => 'article',//类似于库名
            'body' => [
                'mappings' => [

                    'properties' => [
                        //之后可以进行搜索的字段
                        'article_title' => [
            'type' => 'text',
            "analyzer" => "ik_max_word",
            "search_analyzer" => "ik_max_word"
        ]
                    ]
                ]
            ]
        ];
        //执行创建
        $r = $es->indices()->create($params);
        dump($r);
    }
    public function add($id,$data)
    {
        $params = [
            'index' => 'article',
            'type' => '_doc',
            'id' => $id,
            'body' =>$data
        ];
        return $this->client->create($params);
    }
    public function search($data)
    {
        $params = [
            'index' => 'article',
            'type' => '_doc',
            'body' => [
                'query' => [
                    'match' => [
                        'article_title' => $data
                    ]
                ],
                'highlight'=>[
                    'fields'=>[
                        'article_title' => [
                            'pre_tags'=>"<em style='color: red'>",
                            'post_tags'=>"</em>",
                        ]
                    ]
                ]
            ]
        ];
        return $this->client->search($params);
    }
}