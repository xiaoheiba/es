<?php

namespace App\Http\Business;

use Elasticsearch\ClientBuilder;

class Elasticsearch
{
    //ES客户端链接
    private $client;

    /**
     * 构造函数
     *
     * MyElasticsearch constructor
     *
     * */
    public function __construct()
    {
        $this->client = ClientBuilder::create()->setHosts(['127.0.0.1:9200'])->build();
    }


    /**
     *添加索引
     */
     public function add_indexes($index,$type,$field)
     {
         $params = [
             'index'=>$index,
             'body'=>[
                 'mappings'=>[
                      $type => [
                        '_source' => [
                            'enabled' => true
                        ],
                     'properties'=>[
                         $field=>[
                             'type'=>'text',
                             'analyzer'=>'ik_max_word',
                         ]
                     ]
                 ]
                 ]
             ]
         ];
         return $this->client->indices()->create($params);
     }
    /**
     * 添加文档
     * @param $id
     * @param string $index_name
     * @param string $type_name
     * @return array
     */
    public function add_doc($id, $index_name, $type_name, $doc)
    {
        $params = [
            'index' => $index_name,
            'type' => $type_name,
            'id' => $id,
            'body' => $doc
        ];

        return $this->client->index($params);
    }


    /**
     * @param $key
     * @param $index
     * @param $type
     * @return array|callable
     * 上面的搜索是一个范例
     * 本搜索是做es搜索的主要
     */
    public function search($key, $index, $type)
    {
        //搜索的值和库中的值进行对比
        $params = [
            'index' => $index,//数据库名
            'type' => $type, // 表名
            'body' => [
                'query' => [
                    'match' => [
                        //要搜索的字段↓
                        'title' => $key//要搜索的内容
                    ]
                ],
                'highlight' => [ // 高亮
                    'fields' => [
                        'title' => [
                            'pre_tags' => [
                                '<span style="color: red">'
                            ],//样式
                            'post_tags' => [
                                '</span>'
                            ]
                        ]
                    ]
                ]
            ],
        ];
        //执行搜索
        return $this->client->search($params);
    }
}
