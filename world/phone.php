<?php


namespace app;


class phone
{
    public function tel(Request $request)
    {
        $code = $request->param('code');
        $token = $request->param('token');
        $appid = 'wx71a87fdc4cd21422';
        $secret = '8b88542494503612059aa37624d44053';
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appid&secret=$secret";
        $info = file_get_contents($url);
        $arr = json_decode($info, true);
        $access_token = $arr['access_token'];
        $codeUrl = "https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=$access_token";
        $data = ['code' => $code];
        $jsonData = json_encode($data);
        $array = stream_context_create(['http' => ['method' => 'POST', 'header' => "Content-type: application/json", 'content' => $jsonData]]);
        $phoneInfo = file_get_contents($codeUrl, false, $array);
        $results = json_decode($phoneInfo, true);
        $phone = $results['phone_info']['phoneNumber'];
        $user_id = JwtPhp::jwtDecode($token);
        $res = UserModle::where('id', $user_id)->update(['tel' => $phone]);
        if ($res) {
            return json(['code' => 200, 'msg' => '绑定成功', 'data' => []]);
        }

    }
}